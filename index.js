(function() {
  var fs, process, convert;

  fs = require('fs');
  convert = require("xml-js");


  process = require('child_process');

  module.exports = function(photo, callback) {
    var err, killed, stderr, stdout, zbarimg;
    stdout = '';
    stderr = '';
    killed = false;
    if ((photo === null) || (callback === null) || photo.length === 0) {
      err = new Error('Missing parameter');
      callback(err, null);
      return false;
    }
    zbarimg = process.spawn('zbarimg', ["--xml",photo]);
    zbarimg.stdout.setEncoding('utf8');
    zbarimg.stderr.setEncoding('utf8');
    zbarimg.stdout.on('data', function(data) {
      return stdout += data;
    });
    zbarimg.stderr.on('data', function(data) {
      return stderr += data;
    });
    zbarimg.on('error', function(err) {
      if (killed === true) {
        return false;
      }
      killed = true;
      callback(err, null);
      return true;
    });
    return zbarimg.on('close', function(code) {
      var codeName;
      if (killed === true) {
        return false;
      }
      killed = true;
      if (stdout !== null) {
//	console.log(stdout);
	var codeName = convert.xml2json(stdout,{compact:true,fullTagEmptyElement:true,indentAttributes:true});
	codeName = JSON.parse(codeName);
//	console.log(codeName);
        /*codeName = stdout.slice(0, stdout.indexOf(':') + 1);*/
	if (Object.keys(codeName).length>0) {
//		console.dir(codeName,{depth:null,color:true});
		if (codeName.barcodes) {
			if (codeName.barcodes.source) {
				if (codeName.barcodes.source.index) {
					if (codeName.barcodes.source.index.symbol) {
						if (Array.isArray(codeName.barcodes.source.index.symbol)) {
						        if (codeName.barcodes.source.index.symbol.length > 0) {
								  var ut = [];
								  for(var i in codeName.barcodes.source.index.symbol){
									var symb = codeName.barcodes.source.index.symbol[i];
	                                                                var values = {};
	                                                                values.type = symb._attributes.type;
	                                                                values.quality = symb._attributes.quality;
	                                                                values.text = symb.data._cdata;
									ut.push(values);
								  }
							          callback(null, ut);
							          return true;
						        } else {
						        	  err = new Error('No code found or barcode is not supported');
							          callback(err, null);
							          return false;
						        }
						}else{
							if (codeName.barcodes.source.index.symbol) {
								  var symb = codeName.barcodes.source.index.symbol;
								  var values = {};
								  values.type = symb._attributes.type;
								  values.quality = symb._attributes.quality;
								  values.text = symb.data._cdata;
                                                                  callback(null, [values]);
                                                                  return true;
                                                        } else {
                                                                  err = new Error('No code found or barcode is not supported');
                                                                  callback(err, null);
                                                                  return false;
                                                        }
						}
					}else{
						err = new Error('No code found or barcode is not supported');
	                                        callback(err, null);
	                                        return false;
					}
				}else{
					err = new Error('No code found or barcode is not supported');
	                                callback(err, null);
	                                return false;
				}
			}else{
				err = new Error('No code found or barcode is not supported');
	                        callback(err, null);
	                        return false;
			}
		}else{
			err = new Error('No code found or barcode is not supported');
	                callback(err, null);
	                return false;
		}
	}else{
		err = new Error('No code found or barcode is not supported');
                callback(err, null);
                return false;
	}
      }
      if (stderr !== null) {
        err = new Error(stderr);
        callback(err, null);
        return false;
      }
    });
  };

}).call(this);